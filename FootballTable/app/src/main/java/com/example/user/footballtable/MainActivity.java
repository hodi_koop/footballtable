package com.example.user.footballtable;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    // Начальное  количество очков первой и второй команды
    private int firstCount = 0;
    private int secondCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null &&
                savedInstanceState.containsKey("first_team") && savedInstanceState.containsKey("second_team")) {
            firstCount = savedInstanceState.getInt("first_team");
            secondCount = savedInstanceState.getInt("second_team");
            outputCountOfPoints(firstCount, R.id.first_count);
            outputCountOfPoints(secondCount, R.id.second_count);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("first_team", firstCount);
        outState.putInt("second_team", secondCount);
    }

    /**
     * Инкрементирует количество очков у первой команды
     *
     * @param view вьюшка первой команды
     */
    public void onClickFirstTeam(View view) {
        firstCount++;
        outputCountOfPoints(firstCount, R.id.first_count);
    }

    /**
     * Инкрементирует количество очков у второй команды
     *
     * @param view вьюшка второй команды
     */
    public void onClickSecondTeam(View view) {
        secondCount++;
        outputCountOfPoints(secondCount, R.id.second_count);
    }

    /**
     * Очищает количество очков у обеих команд
     *
     * @param view вьюшка reset
     */
    public void onClickReset(View view) {
        firstCount = 0;
        secondCount = 0;
        outputCountOfPoints(firstCount, R.id.first_count);
        outputCountOfPoints(secondCount, R.id.second_count);
    }

    /**
     * Метод выводит количество очков на табло одной из выбранных команд
     * @param command количество очков выбранный команды
     * @param id идентификатор команды, к которой нужно передать количество очков
     */
    public void outputCountOfPoints(int command, int id) {
        TextView counterView = (TextView) findViewById(id);
        counterView.setText(String.format(Locale.getDefault(), "%d", command));
    }
}